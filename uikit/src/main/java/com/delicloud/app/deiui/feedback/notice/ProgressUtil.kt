package com.delicloud.app.deiui.feedback.notice

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Handler
import android.view.View


/**
 * date: 2017/8/7
 * https://lanhuapp.com/web/#/item/project/board/detail?pid=45fd033f-940e-45b6-a6b2-716929ba5ff3&project_id=45fd033f-940e-45b6-a6b2-716929ba5ff3&image_id=50599a99-dfee-4c4d-bbb7-cb63be5113e5
 *05—02
 * 反馈-轻提示
 * desc: ProgressUtil等待,轻提示弹窗工具类
 */

object ProgressUtil {

    private var mProgressHUD: KProgressHUD? = null
    private var mSpecial: KProgressHUD? = null

    /**
     * 默认等待弹窗
     */
    fun show(context: Context?) {
        if (context == null) {
            return
        }

        if (mProgressHUD == null) {
            mProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("请稍等...")
                .setCancellable(false)
                .setFocusable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mProgressHUD!!.show()
                }
            }
        } else if (mProgressHUD!!.context !== context) {
            dismiss()
            mProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("请稍等...")
                .setCancellable(false)
                .setFocusable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mProgressHUD!!.show()
                }
            }
        }
    }


    fun show(
        context: Context?, message: String, canCancelable: Boolean,
        listener: DialogInterface.OnCancelListener
    ) {
        if (context == null) {
            return
        }
        if (mProgressHUD == null) {
            mProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(canCancelable)
                .setCancellable(listener)
                .setFocusable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mProgressHUD!!.show()
                }
            }
        } else if (mProgressHUD!!.context !== context) {
            dismiss()
            mProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(canCancelable)
                .setCancellable(listener)
                .setFocusable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mProgressHUD!!.show()
                }
            }
        }
    }

    fun dismiss() {
        if (mProgressHUD == null) {
            return
        }
        if (mProgressHUD!!.isShowing) {
            try {
                mProgressHUD!!.dismiss()
                mProgressHUD = null
            } catch (e: Exception) {
                // maybe we catch IllegalArgumentException here.
                e.printStackTrace()
            }

        }
    }

    /**
     * 登录弹窗
     *
     * @param context 上下文
     * @param isShow  是否显示dialog
     */
    fun displaySpecialProgress(
        context: Context?, isShow: Boolean,
        listener: DialogInterface.OnCancelListener
    ) {
        if (context == null) {
            return
        }
        if (isShow) {
            mSpecial?.dismiss()
            mSpecial = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("正在登录,请稍候...")
                .setCancellable(true)
                .setCancellable(listener)
                .setFocusable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mSpecial!!.show()
                }
            }
        } else {
            if (mSpecial != null && mSpecial!!.isShowing
                && !(context as Activity).isDestroyed && !context.isFinishing
            ) {
                mSpecial!!.dismiss()
                mSpecial = null
            }
        }
    }

    /**
     * 自动取消弹窗，用于警告，错误，成功轻提示，带图片
     *02-04
     * @param context
     * @param msg
     * @param duration  弹窗显示时长，秒为单位
     * @param view
     */
    fun displayProgressAutoDismiss(context: Context, msg: String, duration: Double = 3.0, view: View) {
        displayCustomViewProgress(
            context, msg, true,
            view, null
        )
        Handler().postDelayed(
            {
                //isShow=false,关闭弹窗
                displayCustomViewProgress(context, "", false, null, null)
            },
            (duration.toInt() * 1000).toLong()
        )
    }

    /**
     * 仅含文本，不带图轻提示,自动消失
     * @param context
     * @param msg
     * @param duration 提示持续时长 s
     */
    fun displayTextAutoDismiss(context: Context?, msg: String, duration: Double = 3.0) {
        if (context == null) {
            return
        }
        mSpecial?.dismiss()
        mSpecial = KProgressHUD.create(context)
            .setLabel(msg)
            .setCancellable(true)
            .setFocusable(false)
            .setDimAmount(0.5f)
        Handler().postDelayed(
            {
                mSpecial?.dismiss()
            },
            (duration.toInt() * 1000).toLong()
        )
        mSpecial?.show()
    }

    /**
     * 自定义信息弹窗
     */
    fun displaySpecialProgress(
        context: Context?, message: String, isShow: Boolean,
        listener: DialogInterface.OnCancelListener
    ) {
        if (context == null) {
            return
        }
        if (isShow) {
            mSpecial = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(true)
                .setCancellable(listener)
                .setFocusable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mSpecial!!.show()
                }
            }
        } else {
            if (mSpecial != null && mSpecial!!.isShowing
                && !(context as Activity).isDestroyed && !context.isFinishing
            ) {
                mSpecial!!.dismiss()
                mSpecial = null
            }
        }
    }

    /**
     * 自定义提示信息上的View
     */
    @Synchronized
    fun displayCustomViewProgress(
        context: Context?, message: String, isShow: Boolean, view: View?,
        listener: DialogInterface.OnCancelListener?
    ) {
        if (context == null) {
            return
        }
        if (isShow) {
            mSpecial?.dismiss()
            mSpecial = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(message)
                .setCancellable(true)
                .setCancellable(listener)
                .setFocusable(true)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
            if (view != null) {
                mSpecial!!.setCustomView(view)
            }
            if (context is Activity) {
                if (!context.isFinishing && !context.isDestroyed) {
                    mSpecial!!.show()
                }
            }
        } else {
            if (mSpecial != null && mSpecial!!.isShowing
                && !(context as Activity).isDestroyed && !context.isFinishing
            ) {
                mSpecial!!.dismiss()
                mSpecial = null
            }
        }
    }
}
