package com.delicloud.app.deiui.feedback.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.view.Display
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams
import android.widget.ScrollView
import android.widget.TextView
import com.delicloud.app.deiui.R


import java.util.ArrayList


/**
 * 05-01-05
 * 底部弹出选择框
 * ActionSheetDialog
 *弹窗目录-系统弹窗-底部弹窗
 *https://lanhuapp.com/web/#/item/project/board/detail?pid=45fd033f-940e-45b6-a6b2-716929ba5ff3&project_id=45fd033f-940e-45b6-a6b2-716929ba5ff3&image_id=f5f8d79a-9d45-41cf-8b94-a48e0c6a8ee2
 * Copy from internet thanks for share
 *
 * @author Irvin 2017-8-23
 */
class DeiUiActionSheetDialog(private val context: Context) {
    private var dialog: Dialog? = null
    private var txt_title: TextView? = null
    private var txt_cancel: TextView? = null
    private var lLayout_content: LinearLayout? = null
    private var sLayout_content: ScrollView? = null
    private var showTitle = false
    private var sheetItemList: MutableList<SheetItem>? = null
    private val display: Display
    private var mOnDissmissListener: OnDissmissListener? = null
    private var isUserCancel = true

    private val sheetItems: List<SheetItem>?
        get() = sheetItemList

    fun setOnDissmissListener(onDissmissListener: OnDissmissListener) {
        mOnDissmissListener = onDissmissListener
    }

    init {
        val windowManager = context
            .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        display = windowManager.defaultDisplay
    }

    fun builder(): DeiUiActionSheetDialog {
        // 获取Dialog布局
        val view = LayoutInflater.from(context).inflate(
            R.layout.toast_view_actionsheet, null
        )

        // 设置Dialog最小宽度为屏幕宽度
        view.minimumWidth = display.width

        // 获取自定义Dialog布局中的控件
        sLayout_content = view.findViewById<View>(R.id.sLayout_content) as ScrollView
        lLayout_content = view
            .findViewById<View>(R.id.lLayout_content) as LinearLayout
        txt_title = view.findViewById<View>(R.id.txt_title) as TextView
        txt_cancel = view.findViewById<View>(R.id.txt_cancel) as TextView
        txt_cancel!!.setOnClickListener { dialog!!.dismiss() }

        // 定义Dialog布局和参数
        dialog = Dialog(context, R.style.DeiUiActionSheetDialogStyle)
        dialog!!.setContentView(view)
        val dialogWindow = dialog!!.window
        dialogWindow!!.setGravity(Gravity.LEFT or Gravity.BOTTOM)
        val lp = dialogWindow.attributes
        lp.x = 0
        lp.y = 0
        dialogWindow.attributes = lp

        return this
    }

    fun setTitle(title: String, color: SheetItemColor?): DeiUiActionSheetDialog {
        showTitle = true
        txt_title!!.visibility = View.VISIBLE
        txt_title!!.text = title
        if (color == null) {
            txt_title!!.setTextColor(context.resources.getColor(R.color.actionsheet_gray))
        } else {
            txt_title!!.setTextColor(Color.parseColor(color.name))
        }
        return this
    }

    fun setCancelColor(color: SheetItemColor?): DeiUiActionSheetDialog {
        if (color == null) {
            txt_cancel!!.setTextColor(
                Color.parseColor(
                    SheetItemColor.Blue
                        .name
                )
            )
        } else {
            txt_cancel!!.setTextColor(Color.parseColor(color.name))
        }
        return this
    }

    fun setCancelable(cancel: Boolean): DeiUiActionSheetDialog {
        dialog!!.setCancelable(cancel)
        return this
    }

    fun setCanceledOnTouchOutside(cancel: Boolean): DeiUiActionSheetDialog {
        dialog!!.setCanceledOnTouchOutside(cancel)
        return this
    }

    /**
     * @param strItem  条目名称
     * @param color    条目字体颜色，设置null则默认蓝色
     * @param listener
     * @return
     */
    fun addSheetItem(
        strItem: String, color: SheetItemColor,
        listener: OnSheetItemClickListener
    ): DeiUiActionSheetDialog {
        if (sheetItemList == null) {
            sheetItemList = ArrayList()
        }
        sheetItemList!!.add(SheetItem(strItem, color, listener))
        return this
    }

    /**
     * 设置条目布局
     */
    private fun setSheetItems() {
        if (sheetItemList == null || sheetItemList!!.size <= 0) {
            return
        }

        val size = sheetItemList!!.size

        // TODO 高度控制，非最佳解决办法
        // 添加条目过多的时候控制高度
        if (size >= 7) {
            val params = sLayout_content!!
                .layoutParams as LayoutParams
            params.height = display.height / 2
            sLayout_content!!.layoutParams = params
        }

        // 循环添加条目
        for (i in 1..size) {
            val sheetItem = sheetItemList!![i - 1]
            val strItem = sheetItem.name
            val color = sheetItem.color
            val listener = sheetItem.itemClickListener

            val textView = TextView(context)
            textView.text = strItem
            textView.textSize = 19f
            textView.gravity = Gravity.CENTER

            // 背景图片
            if (size == 1) {
                if (showTitle) {
                    textView.setBackgroundResource(R.drawable.actionsheet_bottom_selector)
                } else {
                    textView.setBackgroundResource(R.drawable.actionsheet_single_selector)
                }
            } else {
                if (showTitle) {
                    if (i >= 1 && i < size) {
                        textView.setBackgroundResource(R.drawable.actionsheet_middle_selector)
                    } else {
                        textView.setBackgroundResource(R.drawable.actionsheet_bottom_selector)
                    }
                } else {
                    if (i == 1) {
                        textView.setBackgroundResource(R.drawable.actionsheet_top_selector)
                    } else if (i < size) {
                        textView.setBackgroundResource(R.drawable.actionsheet_middle_selector)
                    } else {
                        textView.setBackgroundResource(R.drawable.actionsheet_bottom_selector)
                    }
                }
            }

            // 字体颜色
            if (color == null) {
                textView.setTextColor(
                    Color.parseColor(
                        SheetItemColor.Blue
                            .name
                    )
                )
            } else {
                textView.setTextColor(Color.parseColor(color.name))
            }

            // 高度
            val scale = context.resources.displayMetrics.density
            val height = (45 * scale + 0.5f).toInt()
            textView.layoutParams = LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, height
            )

            // 点击事件
            textView.setOnClickListener {
                isUserCancel = false
                listener.onClick(i)
                dialog!!.dismiss()
            }

            lLayout_content!!.addView(textView)
            dialog!!.setOnDismissListener {
                if (isUserCancel) {
                    if (mOnDissmissListener != null) {
                        mOnDissmissListener!!.onDismiss()
                    }
                }
            }
        }
    }

    fun show() {
        setSheetItems()
        dialog!!.show()
    }

    interface OnSheetItemClickListener {
        fun onClick(which: Int)
    }

    interface OnDissmissListener {
        fun onDismiss()
    }

    private inner class SheetItem internal constructor(
        internal var name: String, internal var color: SheetItemColor?,
        internal var itemClickListener: OnSheetItemClickListener
    )

    enum class SheetItemColor  constructor( var color: String?) {
        Blue("#007AFF"), Red("#FF3B30"), Gray("#999999")
    }
}