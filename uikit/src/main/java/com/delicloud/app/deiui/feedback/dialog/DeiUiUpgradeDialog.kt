package com.delicloud.app.deiui.feedback.dialog

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import com.delicloud.app.deiui.R
import com.delicloud.app.deiui.utils.ScreenUtil

/**
 *05-04-08
 * 申请授权弹窗
 *https://lanhuapp.com/web/#/item/project/board/detail?pid=45fd033f-940e-45b6-a6b2-716929ba5ff3&project_id=45fd033f-940e-45b6-a6b2-716929ba5ff3&image_id=5abd5dc3-2cfd-42e1-8f3f-b0f9833f91f2
 * @author ChengXinPing
 * @time 2018/8/20 9:52
 */
class DeiUiUpgradeDialog(
    private val upgradeHints: List<String>?,
    private val isCanCancel: Boolean=true,
    private val mListener: OnDialogActionListener?
) : DialogFragment() {

    private var mUpgradeHintList: ListView? = null
    private var mUpgradeButton: TextView? = null
    private var mTitleTv: TextView? = null
    private var mContentTitleTv: TextView? = null
    private var mUpgradeCancel: ImageView? = null
    private var dialogIv: ImageView? = null

    private var rootView: View? = null
    private var title: String? = null
    private var contentTitle: String? = null
    private var btnStr: String? = null
    //弹窗drawable
    private var logoDrawable: Drawable? = null
    //弹窗bitmap
    private var logoBitmap: Bitmap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val window = this.dialog.window
        //去掉dialog默认的padding
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = ScreenUtil.getCustomDialogWidth()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        window.setBackgroundDrawable(ColorDrawable())
        rootView = inflater.inflate(R.layout.deiui_dialog_app_upgrade, null)
        initView()
        return rootView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("saved",true)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState!=null&&savedInstanceState.getBoolean("saved")){
            dismiss()
        }
    }

    private fun initView() {
        mUpgradeButton = rootView!!.findViewById(R.id.dialog_upgrade_button)
        mUpgradeHintList = rootView!!.findViewById(R.id.dialog_upgrade_hint_list)
        mUpgradeCancel = rootView!!.findViewById(R.id.dialog_upgrade_cancel)
        mTitleTv = rootView!!.findViewById(R.id.dialog_title)
        dialogIv = rootView!!.findViewById(R.id.dialog_upgrade_iv)
        mContentTitleTv = rootView!!.findViewById(R.id.dialog_content_title)
        val adapter = UpgradeDialogHintListAdapter(context!!, upgradeHints)
        mUpgradeHintList!!.adapter = adapter
        mUpgradeCancel!!.visibility = if (isCanCancel) View.VISIBLE else View.INVISIBLE
        isCancelable = isCanCancel
        mUpgradeCancel!!.setOnClickListener {
            mListener?.onCancel()
            dismiss()
        }
        mUpgradeButton!!.setOnClickListener {
            mListener?.onButtonClick()
            dismiss()
        }
        setTitle(title)
        setBtnStr(btnStr)
        setContentTitle(contentTitle)
        logoBitmap?.let { setLogoBitmap(it) }
        logoDrawable?.let { setLogoDrawable(it) }
    }


    /**
     * 设置弹窗图像drawable
     */
    fun setLogoDrawable(logoDrawable: Drawable) {
        this.logoDrawable = logoDrawable
        if (dialogIv != null) {
            dialogIv!!.setImageDrawable(logoDrawable)
        }
    }

    /**
     * 设置弹窗bitmap，与drawable 2选1
     */
    fun setLogoBitmap(logoBitmap: Bitmap) {
        this.logoBitmap = logoBitmap
        if (dialogIv != null) {
            dialogIv!!.setImageBitmap(logoBitmap)
        }
    }

    fun setTitle(title: String?) {
        this.title = title
        if (mTitleTv != null) {
            mTitleTv!!.text = title
        }
    }

    fun setContentTitle(contentTitle: String?) {
        this.contentTitle = contentTitle
        if (mContentTitleTv != null) {
            mContentTitleTv!!.text = contentTitle
        }
    }

    fun setBtnStr(btnStr: String?) {
        this.btnStr = btnStr
        if (mUpgradeButton != null) {
            mUpgradeButton!!.text = btnStr
        }
    }

    /**
     * 权限列表适配器
     */
    inner class UpgradeDialogHintListAdapter(private val mContext: Context, private val list: List<String>?) :
        BaseAdapter() {

        override fun getCount(): Int {
            return list?.size ?: 0
        }

        override fun getItem(i: Int): Any? {
            return if (list == null) null else list[i]
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            val viewHolder: ViewHolder
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_dialog_upgrade_hint_list, null)
                viewHolder = ViewHolder()
                viewHolder.itemHint = convertView!!.findViewById(R.id.item_dialog_upgrade_hint)
                convertView.tag = viewHolder
            } else {
                viewHolder = convertView.tag as ViewHolder
            }

            viewHolder.itemHint!!.text = list!![position]
            return convertView
        }

        internal inner class ViewHolder {
            var itemHint: AppCompatTextView? = null
        }
    }

    interface OnDialogActionListener {
        fun onButtonClick()

        fun onCancel()
    }
}
