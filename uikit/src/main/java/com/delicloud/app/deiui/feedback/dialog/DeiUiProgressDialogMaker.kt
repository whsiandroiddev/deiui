package com.delicloud.app.deiui.feedback.dialog

import android.content.Context
import android.content.DialogInterface.OnCancelListener
import android.text.TextUtils
import android.util.Log

/**
 * 创建进度条弹窗
 */
object DeiUiProgressDialogMaker {
    private var progressDialog: DeiUiProgressDialog? = null

    val isShowing: Boolean
        get() = progressDialog != null && progressDialog!!.isShowing

    fun showProgressDialog(context: Context, message: String): DeiUiProgressDialog {
        return showProgressDialog(context, null, message, true, null)
    }

    fun showProgressDialog(context: Context, message: String, cancelable: Boolean): DeiUiProgressDialog {
        return showProgressDialog(context, null, message, cancelable, null)
    }

    @Deprecated("")
    fun showProgressDialog(
        context: Context,
        title: String?, message: String, canCancelable: Boolean, listener: OnCancelListener?
    ): DeiUiProgressDialog {

        if (progressDialog == null) {
            progressDialog = DeiUiProgressDialog(context, message)
        } else if (progressDialog!!.context !== context) {
            // maybe existing dialog is running in a destroyed activity cotext
            // we should recreate one
            Log.e(
                "dialog", "there is a leaked window here,orign mContext: "
                        + progressDialog!!.context + " now: " + context
            )
            dismissProgressDialog()
            progressDialog = DeiUiProgressDialog(context, message)
        }

        progressDialog!!.setCancelable(canCancelable)
        progressDialog!!.setOnCancelListener(listener)

        progressDialog!!.show()

        return progressDialog!!
    }

    fun dismissProgressDialog() {
        if (null == progressDialog) {
            return
        }
        if (progressDialog!!.isShowing) {
            try {
                progressDialog!!.dismiss()
                progressDialog = null
            } catch (e: Exception) {
                // maybe we catch IllegalArgumentException here.
            }

        }

    }

    fun setMessage(message: String) {
        if (null != progressDialog && progressDialog!!.isShowing
            && !TextUtils.isEmpty(message)
        ) {
            progressDialog!!.setMessage(message)
        }
    }

    fun updateLoadingMessage(message: String) {
        if (null != progressDialog && progressDialog!!.isShowing
            && !TextUtils.isEmpty(message)
        ) {
            progressDialog!!.updateLoadingMessage(message)
        }
    }
}
