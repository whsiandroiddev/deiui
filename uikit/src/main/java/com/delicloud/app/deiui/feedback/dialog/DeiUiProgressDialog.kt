package com.delicloud.app.deiui.feedback.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.WindowManager.LayoutParams
import android.widget.TextView
import com.delicloud.app.deiui.R

/**
 * 一个半透明窗口,包含一个Progressbar 和 Message部分. 其中Message部分可选. 可单独使用,也可以使用
 * [DeiUiProgressDialogMaker] 进行相关窗口显示.
 *
 * @author Qijun
 */
class DeiUiProgressDialog @JvmOverloads constructor(
    private val mContext: Context,
    style: Int = R.style.deiui_dialog_style,
    private val mLayoutId: Int = R.layout.deiui_progress_dialog
) : Dialog(mContext, style) {

    private var mMessage: String? = null

    private var message: TextView? = null

    init {
        val Params = window!!.attributes
        Params.width = LayoutParams.FILL_PARENT
        Params.height = LayoutParams.FILL_PARENT
        window!!.attributes = Params
    }

    constructor(context: Context, layout: Int, msg: String) : this(context, R.style.deiui_dialog_style, layout) {
        setMessage(msg)
    }

    constructor(context: Context, msg: String) : this(
        context,
        R.style.deiui_dialog_style,
        R.layout.deiui_progress_dialog
    ) {
        setMessage(msg)
    }

    fun setMessage(msg: String) {
        mMessage = msg
    }

    fun updateLoadingMessage(msg: String) {
        mMessage = msg
        showMessage()
    }

    override fun onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        setContentView(mLayoutId)
        message = findViewById<View>(R.id.easy_progress_dialog_message) as TextView
        showMessage()
    }

    private fun showMessage() {
        if (message != null && !TextUtils.isEmpty(mMessage)) {
            message!!.visibility = View.VISIBLE
            message!!.text = mMessage
        }
    }
}