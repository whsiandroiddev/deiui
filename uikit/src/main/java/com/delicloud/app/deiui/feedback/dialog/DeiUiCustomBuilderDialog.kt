package com.delicloud.app.deiui.feedback.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.annotation.IdRes
import com.delicloud.app.deiui.utils.ScreenUtil

/**
 * @author ChengXinPing
 * @time 2018/1/4 15:47
 * 自定义dialog  只需要传入自定义布局和自定义style
 */

class DeiUiCustomBuilderDialog : Dialog {
    private var mContext: Context? = null
    private var cancelTouchout: Boolean = false
    private var view: View? = null

    private constructor(builder: Builder) : super(builder.context) {
        mContext = builder.context
        cancelTouchout = builder.cancelTouchout
        view = builder.view

    }

    private constructor(builder: Builder, resStyle: Int) : super(builder.context, resStyle) {
        mContext = builder.context
        cancelTouchout = builder.cancelTouchout
        view = builder.view
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(view!!)
        setCanceledOnTouchOutside(cancelTouchout)
        val win = window
        val lp = win!!.attributes
        lp.gravity = Gravity.CENTER
        lp.height = ScreenUtil.dip2px(250f)
        lp.width = ScreenUtil.dip2px(275f)
        win.attributes = lp
    }

    fun getView(@IdRes resId: Int): View {
        return view!!.findViewById(resId)
    }

    class Builder(val context: Context) {
        var cancelTouchout: Boolean = false
        var view: View? = null
        var resStyle = -1


        fun cancelTouchout(cancelTouchOut: Boolean): Builder {
            cancelTouchout = cancelTouchOut
            return this
        }

        fun build(): DeiUiCustomBuilderDialog {
            return if (resStyle != -1) {
                DeiUiCustomBuilderDialog(this, resStyle)
            } else {
                DeiUiCustomBuilderDialog(this)
            }
        }

        fun view(resView: Int): Builder {
            view = LayoutInflater.from(context).inflate(resView, null)
            return this
        }

        fun style(resStyle: Int): Builder {
            this.resStyle = resStyle
            return this
        }

        fun addViewOnclick(viewRes: Int, listener: View.OnClickListener): Builder {
            view!!.findViewById<View>(viewRes).setOnClickListener(listener)
            return this
        }

    }

}
