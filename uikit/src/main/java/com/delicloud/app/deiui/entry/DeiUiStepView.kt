package com.delicloud.app.deiui.entry

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.delicloud.app.deiui.R

/**
 * 步进器
 *Created By Mr.m
 *2019/7/25
 **/
open class DeiUiStepView : LinearLayout {
    private lateinit var root: View
    private lateinit var mContext: Context
    //增加button
    private lateinit var incImageView: ImageView
    //减小button
    private lateinit var desImageView: ImageView
    //步数监听
    private var stepChangeListener: OnStepChangeListener? = null
    private lateinit var stepTv: TextView
    //步数下限
    private var min = 0
    //步数上限
    private var max = 99
    //当前步数
    private var step = 10

    constructor(context: Context) : super(context) {
        this.mContext = context
        initView(context)
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        mContext = context
        initView(context)
        initAttr(attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        mContext = context
        initView(context)
        initAttr(attributeSet)
    }

    fun initAttr(attributeSet: AttributeSet) {
        val typeArray = mContext.obtainStyledAttributes(attributeSet, R.styleable.DeiUiStepView, 0, -1)
        try {
            //填充参数
            min = typeArray.getInteger(R.styleable.DeiUiStepView_min, 0)
            max = typeArray.getInteger(R.styleable.DeiUiStepView_max, 99)
            step = typeArray.getInteger(R.styleable.DeiUiStepView_step, 0)
            val textSize = typeArray.getDimension(R.styleable.DeiUiStepView_stepTextSize, 24f)
            stepTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)
            //根据控件大小加载不同的图片，避免失真
            if (typeArray.getInt(R.styleable.DeiUiStepView_size, 0) == 0) {
                //加载小图
                incImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.stepview_inc_selector))
                desImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.stepview_des_selector))
            } else {
                //加载大图
                incImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.stepview_inc_big_selector))
                desImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.stepview_des_big_selector))
            }
            refreshView()
        } finally {
            typeArray.recycle()
        }
    }

    fun initView(context: Context) {
        root = LayoutInflater.from(context).inflate(R.layout.deiui_stepview, this, true)
        incImageView = root.findViewById(R.id.inc_iv)
        desImageView = root.findViewById(R.id.des_iv)
        stepTv = root.findViewById(R.id.step_tv)
        desImageView.setOnClickListener {
            if (step > min) {
                step--
                stepChangeListener?.onStepChange(step)
                //更新视图
                refreshView()
            }
        }
        incImageView.setOnClickListener {
            if (step < max) {
                step++
                stepChangeListener?.onStepChange(step)
                refreshView()
            }
        }
    }

    /**
     * 刷新增加和减小button状态和步数值
     */
    fun refreshView() {
        stepTv.text = "$step"
        desImageView.isEnabled = (step != min)
        incImageView.isEnabled = (step != max)
    }

    /**
     * 设置步数变化监听
     */
    fun setOnStepChangeListener(listener: OnStepChangeListener) {
        this.stepChangeListener = listener
    }

    /**
     * 步数监听
     */
    interface OnStepChangeListener {
        fun onStepChange(step: Int)
    }
}