package com.delicloud.app.deiui.feedback.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.delicloud.app.deiui.R
import com.delicloud.app.deiui.utils.ScreenUtil
import com.delicloud.app.deiui.utils.StringUtil

/**
 * 05-04-06
 * 简单的带有输入框的对话框
 * Created by huangjun on 2015/5/28.股灾
 * https://lanhuapp.com/web/#/item/project/board/detail?pid=45fd033f-940e-45b6-a6b2-716929ba5ff3&project_id=45fd033f-940e-45b6-a6b2-716929ba5ff3&image_id=aa636628-66d8-4839-8b54-97f8f7ad03fb
 */
class DeiUiEditDialogFragment(
    private val mContext: Context?,
    var resourceId: Int,
    private val style: Int
) :
    DialogFragment() {
    private var rootView: View? = null

    private var mTitleTextView: TextView? = null

    private var mMessageTextView: TextView? = null

    private var mEdit: EditText? = null

    private var mLengthTextView: TextView? = null

    private var mPositiveBtn: TextView? = null

    private var mNegativeBtn: TextView? = null

    private var mPositiveBtnListener: View.OnClickListener? = null

    private var mNegativeBtnListener: View.OnClickListener? = null

    private var mTitle: String? = null

    private var mPositiveBtnStrResId = R.string.confirm

    private var mNegativeBtnStrResId = R.string.cancel

    private var mConfirmStr: CharSequence = ""
    private var mCancelStr: CharSequence = ""

    private var mMessage: String? = null

    private var mEditHint: String? = null

    private var mEditString: String? = null

    private var mMaxEditTextLength: Int = 0

    private var mMaxLines = 0

    private var mSingleLine = false

    private var mShowEditTextLength = false

    private var inputType = -1

    private var isNegativeBtnVisible = false

    val editMessage: String?
        get() = if (mEdit != null) {
            mEdit!!.editableText.toString()
        } else {
            null
        }

    init {
        mMaxEditTextLength = 50
    }

    @JvmOverloads
    constructor(
        context: Context?,
        mResourceId: Int = R.layout.deiui_alert_dialog_with_edit_text
    ) : this(
        context,
        mResourceId,
        R.style.deiui_sdk_share_dialog
    )

    fun setTitle(title: String?) {
        if (null != title) {
            this.mTitle = title
            if (null != mTitleTextView) {
                mTitleTextView!!.text = title
            }
        }
    }

    fun setMessage(message: String?) {
        if (null != message) {
            this.mMessage = message
            if (null != mMessageTextView) {
                mMessageTextView!!.text = message
            }
        }
    }

    fun setPositiveBtnText(confirmStr: CharSequence?) {
        if (null != confirmStr) {
            this.mConfirmStr = confirmStr
            if (null != mPositiveBtn) {
                mPositiveBtn!!.text = confirmStr
            }
        }
    }

    fun setNegativeBtnText(cancelStr: CharSequence?) {
        if (null != cancelStr) {
            this.mCancelStr = cancelStr
            if (null != mPositiveBtn) {
                mNegativeBtn!!.text = mCancelStr
            }
        }
    }

    fun setEditHint(hint: String) {
        if (!TextUtils.isEmpty(hint)) {
            this.mEditHint = hint
            if (null != mEdit) {
                mEdit!!.hint = hint
            }
        }
    }

    fun setEditText(EditString: String) {
        if (!TextUtils.isEmpty(EditString)) {
            this.mEditString = EditString
            if (null != mEdit) {
                mEdit!!.setText(EditString)
                mEdit!!.setSelection(EditString.length)
            }
        }
    }

    fun setInputType(type: Int) {
        this.inputType = type
    }

    fun setEditTextMaxLength(maxLength: Int): DeiUiEditDialogFragment {
        this.mMaxEditTextLength = maxLength
        this.mShowEditTextLength = true
        return this
    }

    fun setEditTextMaxLines(maxLines: Int) {
        this.mMaxLines = maxLines
    }

    fun setEditTextSingleLine() {
        this.mSingleLine = true
    }

    fun addPositiveButtonListener(positiveBtnListener: View.OnClickListener) {
        this.mPositiveBtnListener = positiveBtnListener
    }

    fun addPositiveButtonListener(resId: Int, positiveBtnListener: View.OnClickListener) {
        this.mPositiveBtnStrResId = resId
        this.mPositiveBtnListener = positiveBtnListener
    }

    fun addNegativeButtonListener(negativeBtnListener: View.OnClickListener) {
        this.mNegativeBtnListener = negativeBtnListener
    }

    fun addNegativeButtonListener(resId: Int, negativeBtnListener: View.OnClickListener) {
        this.mNegativeBtnStrResId = resId
        this.mNegativeBtnListener = negativeBtnListener
        isNegativeBtnVisible = true
        if (mNegativeBtn != null) {
            if (mNegativeBtnStrResId != 0) {
                mNegativeBtn!!.setText(mNegativeBtnStrResId)
            }
            if (!TextUtils.isEmpty(mCancelStr)) {
                mNegativeBtn!!.text = mCancelStr
            }
            mNegativeBtn!!.setOnClickListener(mNegativeBtnListener)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val window = this.dialog.window
        //去掉dialog默认的padding
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = ScreenUtil.getCustomDialogWidth()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        window.setBackgroundDrawable(ColorDrawable())
        rootView = inflater.inflate(resourceId, null)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("saved", true)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null && savedInstanceState.getBoolean("saved")) {
            //dialog被销毁后重建调用，避免过多逻辑操作，暂时dismiss（）
            dismiss()
        }
    }

    private fun initView() {
        try {
            if (mTitle != null) {
                mTitleTextView = rootView!!.findViewById<View>(R.id.dialog_title_tv) as TextView
                mTitleTextView!!.text = mTitle
            }
            mMessageTextView = rootView!!.findViewById<View>(R.id.desc_tv) as TextView
            if (mMessage != null) {
                mMessageTextView!!.text = mMessage
                mMessageTextView!!.visibility = View.VISIBLE
            } else {
                mMessageTextView!!.visibility = View.GONE
            }
            mEdit = rootView!!.findViewById<View>(R.id.easy_alert_dialog_edit_text) as EditText
            mLengthTextView = rootView!!.findViewById<View>(R.id.edit_text_length) as TextView
            mEdit?.filters = arrayOf(InputFilter.LengthFilter (mMaxEditTextLength))
            mLengthTextView!!.visibility = if (mShowEditTextLength) View.VISIBLE else View.GONE
            if (inputType != -1) {
                mEdit!!.inputType = inputType
            }
            //            mEdit.addTextChangedListener(new EditTextWatcher(mEdit, mLengthTextView, mMaxEditTextLength,
            //                    mShowEditTextLength));

            if (!TextUtils.isEmpty(mEditHint)) {
                mEdit!!.hint = mEditHint
            }

            if (!TextUtils.isEmpty(mEditString)) {
                mEdit!!.setText(mEditString)
                if (mEditString!!.length >= 50) {
                    mEdit!!.setSelection(50)
                } else {
                    mEdit!!.setSelection(mEditString!!.length)
                }
            }
            if (mMaxLines > 0) {
                mEdit!!.maxLines = mMaxLines
            }
            if (mSingleLine) {
                mEdit!!.setSingleLine()
            }

            mPositiveBtn = rootView!!.findViewById<View>(R.id.positive_tv) as TextView
            if (mPositiveBtnStrResId != 0) {
                mPositiveBtn!!.setText(mPositiveBtnStrResId)
            }

            if (!TextUtils.isEmpty(mConfirmStr)) {
                mPositiveBtn!!.text = mConfirmStr
            }
            mPositiveBtn!!.setOnClickListener(mPositiveBtnListener)

            mNegativeBtn = rootView!!.findViewById<View>(R.id.negative_tv) as TextView
            if (isNegativeBtnVisible) {
                mNegativeBtn!!.visibility = View.VISIBLE
                //   rootView.findViewById(R.id.easy_dialog_btn_divide_view).setVisibility(View.VISIBLE);
                if (mNegativeBtnStrResId != 0) {
                    mNegativeBtn!!.setText(mNegativeBtnStrResId)
                }

                if (!TextUtils.isEmpty(mCancelStr)) {
                    mNegativeBtn!!.text = mCancelStr
                }
                mNegativeBtn!!.setOnClickListener(mNegativeBtnListener)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    class EditTextWatcher(
        private val editText: EditText?,
        private val lengthTV: TextView?,
        private val maxLength: Int,
        show: Boolean
    ) : TextWatcher {

        private var show = false

        init {
            this.show = show
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            if (editText == null) {
                return
            }
            var editStart = editText.selectionStart
            var editEnd = editText.selectionEnd
            editText.removeTextChangedListener(this)
            while (StringUtil.counterChars(s.toString()) > maxLength) {
                s.delete(editStart - 1, editEnd)
                editStart--
                editEnd--
            }
            editText.setSelection(editStart)
            editText.addTextChangedListener(this)
            if (show && lengthTV != null) {
                val remainLength = (maxLength - StringUtil.counterChars(s.toString())).toLong()
                lengthTV.text = "" + remainLength / 2
                lengthTV.visibility = View.VISIBLE
            }
        }
    }
}
