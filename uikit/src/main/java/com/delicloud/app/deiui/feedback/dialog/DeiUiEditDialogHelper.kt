package com.delicloud.app.deiui.feedback.dialog

import android.content.Context
import android.text.TextUtils
import android.view.View
import com.delicloud.app.deiui.R

/**
 *
 * Created by Irvin
 * Date: on 2018/7/6 0006.
 */

object DeiUiEditDialogHelper {


    /**
     * 输入弹窗
     */
    fun createEditDialog(
        context: Context,
        titleString: CharSequence,
        hintString: CharSequence,
        confirmString: CharSequence,
        cancelString: CharSequence,
        cancelable: Boolean,
        listener: OnDialogActionListener
    ): DeiUiEditDialogFragment {
        val dialog = DeiUiEditDialogFragment(context)
        dialog.setTitle(titleString as String)
        dialog.setEditHint(hintString as String)
        dialog.setNegativeBtnText(cancelString)
        dialog.setPositiveBtnText(confirmString)
        dialog.addNegativeButtonListener(R.string.cancel, View.OnClickListener {
            dialog.dismiss()
            listener.doCancelAction()
        })
        dialog.addPositiveButtonListener(R.string.confirm, View.OnClickListener {
            dialog.dismiss()
            listener.doOkAction(dialog.editMessage)
        })
        dialog.isCancelable = cancelable
        return dialog
    }

    /**
     * 输入框默认值弹窗
     */
    fun createEditDialog(
        context: Context,
        titleString: CharSequence,
        hintString: CharSequence,
        editString: CharSequence,
        cancelable: Boolean,
        listener: OnDialogActionListener
    ): DeiUiEditDialogFragment {
        val dialog = DeiUiEditDialogFragment(context)
        dialog.setTitle(titleString as String)
        if (!TextUtils.isEmpty(editString)) {
            dialog.setEditHint(hintString as String)
        }
        dialog.setEditText(editString as String)
        dialog.addNegativeButtonListener(R.string.cancel, View.OnClickListener {
            dialog.dismiss()
            listener.doCancelAction()
        })
        dialog.addPositiveButtonListener(R.string.confirm, View.OnClickListener {
            dialog.dismiss()
            listener.doOkAction(dialog.editMessage)
        })
        dialog.isCancelable = cancelable
        return dialog
    }

    interface OnDialogActionListener {
        fun doCancelAction()

        fun doOkAction(msg: String?)
    }
}
