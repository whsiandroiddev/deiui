package com.delicloud.app.deiui.feedback.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Pair
import android.view.*
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.DialogFragment
import com.delicloud.app.deiui.R
import com.delicloud.app.deiui.feedback.adapter.TAdapter
import com.delicloud.app.deiui.feedback.adapter.TAdapterDelegate
import com.delicloud.app.deiui.feedback.adapter.TViewHolder
import com.delicloud.app.deiui.utils.ScreenUtil

import java.util.LinkedList

/**
 * 05-04-03,04
 * 列表弹窗
 * https://lanhuapp.com/web/#/item/project/board/detail?pid=45fd033f-940e-45b6-a6b2-716929ba5ff3&project_id=45fd033f-940e-45b6-a6b2-716929ba5ff3&image_id=3ee521f0-c1de-4b22-8be4-953aca53446f
 */
class DeiUiListItemDialogFragment : DialogFragment {

    private var mContext: Context? = null

    private var itemSize = 0

    private var titleView: View? = null

    private var rootView: View? = null

    private var titleTextView: TextView? = null
    private var messageTextView: TextView? = null

    private var titleBtn: ImageButton? = null

    private var listView: ListView? = null

    private var isTitleVisible = false

    private var isTitleBtnVisible = false
    private var isMessageVisible = false

    private var title: String? = null
    var message: String? = null
        set(message) {
            field = message
            isMessageVisible = !TextUtils.isEmpty(message)
            setMessageVisible(isMessageVisible)
            if (messageTextView != null) {
                messageTextView!!.text = message
            }
        }

    private var titleListener: View.OnClickListener? = null

    private val itemTextList = LinkedList<Pair<String, Int>>()

    private val itemListenerList = LinkedList<onSeparateItemClickListener>()

    private var listListener: View.OnClickListener? = null

    private var listAdapter: BaseAdapter? = null

    private var itemListener: OnItemClickListener? = null
    private val defaultColor = R.color.deiui_high_level_text_color

    constructor(context: Context?) {
        this.mContext = context
        initAdapter()
    }

    constructor(context: Context, itemSize: Int) {
        this.mContext = context
        this.itemSize = itemSize
    }

    private fun initAdapter() {
        listAdapter = TAdapter(mContext!!, itemTextList, object : TAdapterDelegate {
            override fun getViewTypeCount(): Int {
             return  itemListenerList.size
            }


            override fun viewHolderAtPosition(position: Int): Class<out TViewHolder> {
                return DeiUiListDialogViewHolder::class.java
            }

            override fun enabled(position: Int): Boolean {
                return true
            }
        })
        itemListener = OnItemClickListener { parent, view, position, id ->
            itemListenerList[position].onClick()
            dismiss()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("saved",true)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState!=null&&savedInstanceState.getBoolean("saved")){
            dismiss()
        }
    }

    fun setAdapter(adapter: BaseAdapter, listener: View.OnClickListener) {
        listAdapter = adapter
        listListener = listener
        itemListener = OnItemClickListener { parent, view, position, id ->
            dismiss()
            listListener!!.onClick(view)
        }
    }

    fun setAdapter(adapter: BaseAdapter, listener: OnItemClickListener) {
        listAdapter = adapter
        itemListener = listener
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val window = this.dialog.window
        //去掉dialog默认的padding
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = ScreenUtil.getCustomDialogWidth()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        window.setBackgroundDrawable(ColorDrawable())
        rootView = inflater.inflate(R.layout.deiui_alert_dialog_with_listview, null)
        initView()
        return rootView
    }

    private fun initView() {
        titleView = rootView!!.findViewById(R.id.easy_dialog_title_view)
        if (titleView != null) {
            setTitleVisible(isTitleVisible)
        }
        titleTextView = rootView!!.findViewById<View>(R.id.dialog_title_tv) as TextView
        if (titleTextView != null) {
            setTitle(title)
        }
        messageTextView = rootView!!.findViewById<View>(R.id.desc_tv) as TextView
        if (messageTextView != null) {
            message = this.message
        }
        titleBtn = rootView!!.findViewById<View>(R.id.easy_dialog_title_button) as ImageButton
        if (titleBtn != null) {
            setTitleBtnVisible(isTitleBtnVisible)
            setTitleBtnListener(titleListener)
        }
        listView = rootView!!.findViewById<View>(R.id.easy_dialog_list_view) as ListView

        if (!isMessageVisible && !isTitleVisible) {
            //标题和描述信息都不可见时，去掉标题描述信息，更改文字样式
            titleView!!.visibility = View.GONE
            DeiUiListDialogViewHolder.TEXT_APPEARANCE_RES_ID = R.style.deiui_list_without_title_dialog_message_text_style
        } else {
            DeiUiListDialogViewHolder.TEXT_APPEARANCE_RES_ID = R.style.deiui_list_with_title_dialog_message_text_style
        }
        if (itemSize > 0) {
            updateListView()
        }
    }

    protected fun addFootView(parent: LinearLayout) {

    }

    /**
     * 设置标题，默认不显示，设置后显示
     */
    fun setTitle(title: String?) {
        this.title = title
        isTitleVisible = !TextUtils.isEmpty(title)
        setTitleVisible(isTitleVisible)
        if (isTitleVisible && titleTextView != null) {
            titleTextView!!.text = title
        }
    }

    fun setTitle(resId: Int) {
        this.title = mContext!!.getString(resId)
        isTitleVisible = if (TextUtils.isEmpty(title)) false else true
        setTitleVisible(isTitleVisible)
        if (isTitleVisible && titleTextView != null) {
            titleTextView!!.text = title
        }
    }

    fun setTitleVisible(visible: Boolean) {
        isTitleVisible = visible
        if (titleView != null) {
            titleView!!.visibility = if (isTitleVisible) View.VISIBLE else View.GONE
        }
    }

    fun setMessageVisible(visible: Boolean) {
        isMessageVisible = visible
        if (messageTextView != null) {
            messageTextView!!.visibility = if (visible) View.VISIBLE else View.GONE
        }
    }

    fun setTitleBtnVisible(visible: Boolean) {
        isTitleBtnVisible = visible
        if (titleBtn != null) {
            titleBtn!!.visibility = if (isTitleBtnVisible) View.VISIBLE else View.GONE
        }
    }

    fun setTitleBtnListener(titleListener: View.OnClickListener?) {
        this.titleListener = titleListener
        if (titleListener != null && titleBtn != null) {
            titleBtn!!.setOnClickListener(titleListener)
        }
    }

    fun addItem(itemText: String, listener: onSeparateItemClickListener) {
        addItem(itemText, defaultColor, listener)
    }

    fun addItem(itemText: String, color: Int, listener: onSeparateItemClickListener) {
        itemTextList.add(Pair(itemText, color))
        itemListenerList.add(listener)
        itemSize = itemTextList.size
    }

    fun addItem(resId: Int, listener: onSeparateItemClickListener) {
        addItem(mContext!!.getString(resId), listener)
    }

    fun addItem(resId: Int, color: Int, listener: onSeparateItemClickListener) {
        addItem(mContext!!.getString(resId), color, listener)
    }

    fun addItemAfterAnother(itemText: String, another: String, listener: onSeparateItemClickListener) {
        val index = itemTextList.indexOfFirst { it.first == another }
        itemTextList.add(index + 1, Pair(itemText, defaultColor))
        itemListenerList.add(index + 1, listener)
        itemSize = itemTextList.size
    }

    fun clearData() {
        itemTextList.clear()
        itemListenerList.clear()
        itemSize = 0
    }

    private fun updateListView() {
        listAdapter!!.notifyDataSetChanged()
        if (listView != null) {
            listView!!.adapter = listAdapter
            listView!!.onItemClickListener = itemListener
        }
    }

    interface onSeparateItemClickListener {
        fun onClick()
    }

    /*  @Override
    public void show() {
        if (itemSize <= 0) {
            return;
        }
        updateListView();
        super.show();
    }*/

}
