package com.delicloud.app.deiui.entry

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.delicloud.app.deiui.R

/**
 * 开关按钮
 *
 * @author sunyoujun
 */
class DeiUiSwitchButton : View, View.OnTouchListener {
    /**
     * 记录当前按钮是否打开,true为打开, false为关闭
     */
    /**
     * 判断开关是否打开
     *
     * @return 开关的状态
     */
    var isChoose = false
        private set

    private var isChecked: Boolean = false
    /**
     * 记录用户是否在滑动的变量
     */
    private var onSlip = false
    /**
     * 按下时的x,当前的x
     */
    private var down_x: Float = 0.toFloat()
    private var now_x: Float = 0.toFloat()
    /**
     * 打开和关闭状态下,游标的Rect .
     */
    private var btn_off: Rect? = null
    private var btn_on: Rect? = null

    private var isChangeOn = false

    private var isInterceptOn = false

    private var onChangedListener: OnChangedListener? = null

    private var bg_on: Bitmap? = null
    private var bg_off: Bitmap? = null
    private var slip_btn: Bitmap? = null

    var check: Boolean
        get() = this.isChecked
        set(isChecked) {
            this.isChecked = isChecked
            isChoose = isChecked
            if (!isChecked) {
                now_x = 0f
            }
            invalidate()
        }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }



    private fun init() {// 初始化
        bg_on = BitmapFactory.decodeResource(resources, R.drawable.custom_sw_slide_toggle_on)
        bg_off = BitmapFactory.decodeResource(resources, R.drawable.custom_sw_slide_toggle_off)
        slip_btn = BitmapFactory.decodeResource(resources, R.drawable.custom_sw_slide_toggle)
        btn_off = Rect(0, 0, slip_btn!!.width, slip_btn!!.height)
        btn_on = Rect(bg_off!!.width - slip_btn!!.width, 0, bg_off!!.width, slip_btn!!.height)
        //设置监听器,也可以直接复写OnTouchEvent
        setOnTouchListener(this)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {// 绘图函数

        super.onDraw(canvas)

        val matrix = Matrix()
        val paint = Paint()
        var x: Float
        // 滑动到前半段与后半段的背景不同,在此做判断
        if (now_x < bg_on!!.width / 2) {
            x = now_x - slip_btn!!.width / 2
            // 画出关闭时的背景
            canvas.drawBitmap(bg_off!!, matrix, paint)
        } else {
            x = (bg_on!!.width - slip_btn!!.width / 2).toFloat()
            // 画出打开时的背景
            canvas.drawBitmap(bg_on!!, matrix, paint)
        }
        // 是否是在滑动状态
        if (onSlip) {
            // 是否划出指定范围,不能让游标跑到外头,必须做这个判断
            if (now_x >= bg_on!!.width) {
                // 减去游标1/2的长度...
                x = (bg_on!!.width - slip_btn!!.width / 2).toFloat()
            } else if (now_x < 0) {
                x = 0f
            } else {
                x = now_x - slip_btn!!.width / 2
            }
        } else {// 非滑动状态
            if (isChoose) {
                // 根据现在的开关状态设置画游标的位置
                x = btn_on!!.left.toFloat()
                // 初始状态为true时应该画出打开状态图片
                canvas.drawBitmap(bg_on!!, matrix, paint)
            } else {
                x = btn_off!!.left.toFloat()
            }
        }
        if (isChecked) {
            canvas.drawBitmap(bg_on!!, matrix, paint)
            x = btn_on!!.left.toFloat()
            isChecked = !isChecked
        }

        // 对游标位置进行异常判断...
        if (x < 0) {
            x = 0f
        } else if (x > bg_on!!.width - slip_btn!!.width) {
            x = (bg_on!!.width - slip_btn!!.width).toFloat()
        }
        // 画出游标.
        canvas.drawBitmap(slip_btn!!, x, 0f, paint)
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {

        if (!isInterceptOn) {
            val old = isChoose
            when (event.action) {
                // 滑动
                MotionEvent.ACTION_MOVE -> now_x = event.x
                // 按下
                MotionEvent.ACTION_DOWN -> {
                    if (event.x > bg_on!!.width || event.y > bg_on!!.height) {
                        return false
                    }
                    onSlip = true
                    down_x = event.x
                    now_x = down_x
                }
                // 移到控件外部
                MotionEvent.ACTION_CANCEL -> {
                    onSlip = false
                    val choose = isChoose
                    if (now_x >= bg_on!!.width / 2) {
                        now_x = (bg_on!!.width - slip_btn!!.width / 2).toFloat()
                        isChoose = true
                    } else {
                        now_x = now_x - slip_btn!!.width / 2
                        isChoose = false
                    }
                    // 如果设置了监听器,就调用其方法..
                    if (isChangeOn && choose != isChoose) {
                        onChangedListener!!.OnChanged(this, isChoose)
                    }
                }
                // 松开
                MotionEvent.ACTION_UP -> {
                    onSlip = false
                    val lastChoose = isChoose
                    if (event.x >= bg_on!!.width / 2) {
                        now_x = (bg_on!!.width - slip_btn!!.width / 2).toFloat()
                        isChoose = true
                    } else {
                        now_x -= slip_btn!!.width / 2
                        isChoose = false
                    }
                    // 相等表示点击状态未切换，之后切换状态
                    if (lastChoose == isChoose) {
                        if (event.x >= bg_on!!.width / 2) {
                            now_x = 0f
                            isChoose = false
                        } else {
                            now_x = (bg_on!!.width - slip_btn!!.width / 2).toFloat()
                            isChoose = true
                        }
                    }
                    // 如果设置了监听器,就调用其方法..
                    if (isChangeOn) {
                        onChangedListener!!.OnChanged(this, isChoose)
                    }
                }
            }
            //        if (!old && isInterceptOn) {
            //            isChoose = false;
            //        } else {
            //            invalidate();// 重画控件
            //        }
            invalidate()// 重画控件
        }
        return true
    }


    fun setOnChangedListener(listener: OnChangedListener) {// 设置监听器,当状态修改的时候
        isChangeOn = true
        onChangedListener = listener
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        //设置透明度
        alpha = if (enabled) 1f else 0.5f
    }

    interface OnChangedListener {
        fun OnChanged(v: View, checkState: Boolean)
    }

    fun setInterceptState(isIntercept: Boolean) {// 设置监听器,是否在重画钱拦截事件,状态由false变true时 拦截事件
        isInterceptOn = isIntercept
        // onInterceptListener = listener;
    }
}

