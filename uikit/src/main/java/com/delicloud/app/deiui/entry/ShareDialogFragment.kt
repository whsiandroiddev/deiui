package com.delicloud.app.deiui.entry

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.gridlayout.widget.GridLayout
import com.delicloud.app.deiui.R
import org.jetbrains.anko.collections.forEachWithIndex


/**
 *
 * 分享控件
 * Created By Mr.m
 * 2019/7/26
 *
 * title 分享标题
 * columnCount 分享弹窗列数 通常为2或3
 */

class ShareDialogFragment(private val mContext: Context, private val titleStr: String, private val columnCount: Int) :
    DialogFragment() {
    lateinit var rootView: View
    /**
     * 标题
     */
    lateinit var titleTv: TextView
    /**
     * 分享item父布局
     */
    lateinit var contentGridLayout: GridLayout
    /**
     * 应用列表
     */
    val shareItems = arrayListOf<ShareItem>()
    var listener: OnShareItemClickListener? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val window = this.dialog.window
        //去掉dialog默认的padding
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        window.setGravity(Gravity.BOTTOM)
        window.setBackgroundDrawable(ColorDrawable())
        rootView = inflater.inflate(R.layout.deiui_sharelayout, null)
        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.DeiUiActionSheetDialogStyle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    fun initView() {
        titleTv = rootView.findViewById(R.id.share_title_tv)
        titleTv.text = titleStr
        contentGridLayout = rootView.findViewById(R.id.share_gridlayout)
        //设置columnCount
        contentGridLayout.columnCount = this.columnCount
        shareItems.forEachWithIndex { index, item ->
            //新建itemView
            val shareItemView = LayoutInflater.from(mContext).inflate(R.layout.item_share_layout, contentGridLayout, false)
            shareItemView.apply {
                findViewById<ImageView>(R.id.item_share_iv).setImageResource(item.appDrawable)
                findViewById<TextView>(R.id.item_share_tv).text = item.appName
                setOnClickListener {
                    listener?.onClick(index,item.appName)
                    dismiss()
                }
            }
            //将item添加到contentGridLayout
            contentGridLayout.addView(shareItemView)
        }
    }

    fun setOnShareItemClickListener(listener: OnShareItemClickListener) {
        this.listener = listener
    }
    fun setItemList(list: List<ShareItem>){
        shareItems.clear()
        shareItems.addAll(list)
    }

    fun addItemView(shareItem: ShareItem) {
        shareItems.add(shareItem)
    }

    /**
     * 分享应用图片和名称
     */
    class ShareItem(val appDrawable: Int, val appName: String)

    /**
     * 点击监听
     */
    interface OnShareItemClickListener {
        fun onClick(position: Int,appName: String)
    }
}
