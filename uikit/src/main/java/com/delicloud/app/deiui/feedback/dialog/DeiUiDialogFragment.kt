package com.delicloud.app.deiui.feedback.dialog

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.delicloud.app.deiui.R
import com.delicloud.app.deiui.utils.ScreenUtil
import java.util.*
import java.util.Map

/**
 * 05-04，01,02,04,05,07,09,10
 * 普通提示和带图提示，以及水平进度条提示
 * 普通提示包含两个按钮以及Title和Message(居左显示).
 * https://lanhuapp.com/web/#/item/project/board?pid=45fd033f-940e-45b6-a6b2-716929ba5ff3
 * 特殊布局需求可以自定义布局.
 */
class DeiUiDialogFragment @JvmOverloads constructor(
    private val mContext: Context?,
    resourceId: Int = R.layout.deiui_alert_dialog_default,
    private val style: Int = R.style.deiui_dialog_default_style
) : DialogFragment() {
    constructor() : this(null)

    private var titleTV: TextView? = null

    private var messageTV: TextView? = null
    /**
     * 带图弹窗 ImageView
     */
    private var imageView: ImageView? = null
    private var rootView: View? = null
    /**
     * ImageView设置Drawable或bitmap
     */
    private var drawable: Drawable? = null
    private var bitmap: Bitmap? = null
    /**
     * 带进度条弹窗，默认不显示，
     */
    private var progressBar: ProgressBar? = null
    private var maxProgress: Int = 100
    private var progress: Int = 0
    var positiveTv: TextView? = null
    var negativeTv: TextView? = null
    private var title: CharSequence = ""
    private var message: CharSequence = ""
    private var positiveBtnTitle: CharSequence = ""
    private var negativeBtnTitle: CharSequence = ""

    private var titleTextColor = NO_TEXT_COLOR
    private var msgTextColor = NO_TEXT_COLOR
    private var positiveBtnTitleTextColor = NO_TEXT_COLOR
    private var negativeBtnTitleTextColor = NO_TEXT_COLOR

    private var titleTextSize = NO_TEXT_SIZE.toFloat()
    private var msgTextSize = NO_TEXT_SIZE.toFloat()
    private var positiveBtnTitleTextSize = NO_TEXT_SIZE.toFloat()
    private var negativeBtnTitleTextSize = NO_TEXT_SIZE.toFloat()

    var resourceId: Int = 0

    private var isPositiveBtnVisible = true
    private var isNegativeBtnVisible = false

    private var isTitleVisible = false
    private var isMessageVisible = true
    private var isProgressBarVisible = false

    private var positiveBtnListener: View.OnClickListener? = null
    private var negativeBtnListener: View.OnClickListener? = null

    private val mViewListener = HashMap<Int, View.OnClickListener>()

    init {
        if (-1 != resourceId) {
            this.resourceId = resourceId
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null && savedInstanceState.getBoolean("saved")) {
            dismiss()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("saved", true)
    }


    fun setTitle(title: CharSequence?) {
        isTitleVisible = !TextUtils.isEmpty(title)
        setTitleVisible(isTitleVisible)
        if (null != title) {
            this.title = title
            if (null != titleTV) {
                titleTV!!.text = title
            }
        }
    }

    fun setDrawable(drawable: Drawable?) {
        this.drawable = drawable
        if (imageView != null) {
            imageView!!.setImageDrawable(drawable)
        }
    }

    fun setDrawableRes(res: Int) {
        setDrawable(ContextCompat.getDrawable(mContext!!, res))
    }

    fun setImageBitmap(bitmap: Bitmap) {
        this.bitmap = bitmap
        if (imageView != null) {
            imageView!!.setImageBitmap(bitmap)
        }
    }

    fun setTitleVisible(visible: Boolean) {
        isTitleVisible = visible
        if (titleTV != null) {
            titleTV!!.visibility = if (visible) View.VISIBLE else View.GONE
        }
    }

    /**
     * 设置水平进度条进度
     */
    fun setProgress(p: Int) {
        //设置进度条可见，默认不可见
        isProgressBarVisible = true
        progress = p
        if (progressBar != null) {
            progressBar!!.progress = p
        }
    }

    fun setMaxProgress(max: Int) {
        maxProgress = max
        if (progressBar != null) {
            progressBar?.max = max
        }
    }

    fun setTitleTextColor(color: Int) {
        titleTextColor = color
        if (null != titleTV && NO_TEXT_COLOR != color) {
            titleTV!!.setTextColor(color)
        }
    }

    fun setMessageTextColor(color: Int) {
        msgTextColor = color
        if (null != messageTV && NO_TEXT_COLOR != color) {
            messageTV!!.setTextColor(color)
        }

    }

    fun setMessageTextSize(size: Float) {
        msgTextSize = size
        if (null != messageTV && NO_TEXT_SIZE.toFloat() != size) {
            messageTV!!.textSize = size
        }
    }

    fun setTitleTextSize(size: Float) {
        titleTextSize = size
        if (null != titleTV && NO_TEXT_SIZE.toFloat() != size) {
            titleTV!!.textSize = size
        }
    }

    fun setMessageVisible(visible: Boolean) {
        isMessageVisible = visible
        if (messageTV != null) {
            messageTV!!.visibility = if (visible) View.VISIBLE else View.GONE
        }
    }

    fun setMessage(message: CharSequence?) {
        if (null != message) {
            this.message = message
            if (null != messageTV) {
                messageTV!!.text = message
            }
        }
    }

    fun addPositiveButton(
        title: CharSequence, color: Int, size: Float,
        positiveBtnListener: View.OnClickListener
    ) {
        isPositiveBtnVisible = true
        positiveBtnTitle = if (TextUtils.isEmpty(title))
            mContext!!.getString(R.string.confirm)
        else
            title
        positiveBtnTitleTextColor = color
        positiveBtnTitleTextSize = size
        this.positiveBtnListener = positiveBtnListener
        /* if (positiveTv != null) {
            positiveTv.setText(positiveBtnTitle);
            positiveTv.setTextColor(positiveBtnTitleTextColor);
            positiveTv.setTextSize(positiveBtnTitleTextSize);
            positiveTv.setOnClickListener(positiveBtnListener);
        }*/
    }

    fun addNegativeButton(
        title: CharSequence, color: Int, size: Float,
        negativeBtnListener: View.OnClickListener
    ) {
        isNegativeBtnVisible = true
        negativeBtnTitle = if (TextUtils.isEmpty(title))
            mContext!!.getString(R.string.cancel)
        else
            title
        negativeBtnTitleTextColor = color
        negativeBtnTitleTextSize = size
        this.negativeBtnListener = negativeBtnListener
    }

    fun addPositiveButton(
        title: CharSequence,
        positiveBtnListener: View.OnClickListener
    ) {
        addPositiveButton(
            title, NO_TEXT_COLOR, NO_TEXT_SIZE.toFloat(),
            positiveBtnListener
        )
    }

    fun addNegativeButton(
        title: CharSequence,
        negativeBtnListener: View.OnClickListener
    ) {
        addNegativeButton(
            title, NO_TEXT_COLOR, NO_TEXT_SIZE.toFloat(),
            negativeBtnListener
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val window = this.dialog.window
        //去掉dialog默认的padding
        window!!.decorView.setPadding(0, 0, 0, 0)
        val lp = window.attributes
        lp.width = ScreenUtil.getCustomDialogWidth()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
        window.setBackgroundDrawable(ColorDrawable())
        rootView = inflater.inflate(resourceId, null)
        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, style)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        try {
            titleTV = rootView!!.findViewById<View>(R.id.dialog_title_tv) as TextView
            if (titleTV != null) {
                titleTV!!.text = title
                setTitleVisible(isTitleVisible)
                if (NO_TEXT_COLOR != titleTextColor) {
                    titleTV!!.setTextColor(titleTextColor)
                }
                if (NO_TEXT_SIZE.toFloat() != titleTextSize) {
                    titleTV!!.textSize = titleTextSize
                }
            }
            messageTV = rootView!!.findViewById<View>(R.id.desc_tv) as TextView
            if (messageTV != null) {
                messageTV!!.text = message
                setMessageVisible(isMessageVisible)
                if (NO_TEXT_COLOR != msgTextColor) {
                    messageTV!!.setTextColor(msgTextColor)
                }
                if (NO_TEXT_SIZE.toFloat() != msgTextSize) {
                    messageTV!!.textSize = msgTextSize
                }
            }
            positiveTv = rootView!!.findViewById(R.id.positive_tv)
            if (isPositiveBtnVisible && positiveTv != null) {
                positiveTv!!.visibility = View.VISIBLE
                if (NO_TEXT_COLOR != positiveBtnTitleTextColor) {
                    positiveTv!!.setTextColor(positiveBtnTitleTextColor)
                }
                if (NO_TEXT_SIZE.toFloat() != positiveBtnTitleTextSize) {
                    positiveTv!!.textSize = positiveBtnTitleTextSize
                }
                positiveTv!!.text = positiveBtnTitle
                positiveTv!!.setOnClickListener(positiveBtnListener)
            }

            negativeTv = rootView!!.findViewById(R.id.negative_tv)
            if (isNegativeBtnVisible) {
                negativeTv!!.visibility = View.VISIBLE
                if (NO_TEXT_COLOR != this.negativeBtnTitleTextColor) {
                    negativeTv!!.setTextColor(negativeBtnTitleTextColor)
                }
                if (NO_TEXT_SIZE.toFloat() != this.negativeBtnTitleTextSize) {
                    negativeTv!!.textSize = negativeBtnTitleTextSize
                }
                negativeTv!!.text = negativeBtnTitle
                negativeTv!!.setOnClickListener(negativeBtnListener)
            }
            imageView = rootView!!.findViewById(R.id.dialog_iv)
            if (imageView != null) {
                if (bitmap != null) {
                    imageView!!.setImageBitmap(bitmap)
                }
                if (drawable != null) {
                    imageView!!.setImageDrawable(drawable)
                }
            }
            val imageView = rootView!!.findViewById<ImageView>(R.id.close_iv)
            imageView?.setOnClickListener { dismiss() }
            if (mViewListener != null && mViewListener.size != 0) {
                val iter = mViewListener.entries.iterator()
                var view: View? = null
                while (iter.hasNext()) {
                    val entry = iter.next() as Map.Entry<Int, View.OnClickListener>
                    view = rootView!!.findViewById(entry.key)
                    if (view != null && entry.value != null) {
                        view.setOnClickListener(entry.value)
                    }
                }
            }
            progressBar = rootView!!.findViewById(R.id.dialog_progress_bar)
            if (isProgressBarVisible && progressBar != null) {
                initProgressDialog()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun initProgressDialog() {
        progressBar!!.visibility = View.VISIBLE
        progressBar!!.isIndeterminate = false
        progressBar!!.max = maxProgress
        progressBar!!.progress = progress
        dialog.setCanceledOnTouchOutside(false)
        dialog.setOnKeyListener { _, i, _ ->
            i == KeyEvent.KEYCODE_BACK
        }
    }

    fun setViewListener(viewId: Int, listener: View.OnClickListener) {
        mViewListener[viewId] = listener
    }

    companion object {

        val NO_TEXT_COLOR = -99999999

        val NO_TEXT_SIZE = -99999999
    }
}
