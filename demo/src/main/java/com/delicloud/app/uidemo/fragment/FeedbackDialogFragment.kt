package com.delicloud.app.uidemo.fragment


import android.view.View
import androidx.core.content.ContextCompat
import com.delicloud.app.deiui.feedback.dialog.*
import com.delicloud.app.uidemo.R
import com.delicloud.app.uidemo.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_feedback_dialog.*

/**
 * 弹窗界面
 */
class FeedbackDialogFragment : BaseFragment() {


    override fun layoutId(): Int {
        return R.layout.fragment_feedback_dialog
    }

    override fun initView() {
        single_btn_dialog_btn.setOnClickListener { showOneButton() }
        double_btn_dialog_btn.setOnClickListener { showDoubleButton() }
        multi_btn_dialog_btn.setOnClickListener { showMultiBtnDialog(true) }
        multi_btn_list_dialog_btn.setOnClickListener { showMultiBtnDialog(false) }
        without_title_dialog_btn.setOnClickListener { showDoubleButtonWithoutTitle() }
        with_img_dialog_btn.setOnClickListener { showDialogWithImg() }
        with_big_img_dialog_btn.setOnClickListener { showDoubleButtonWithBigImage() }
        double_btn_quote_dialog_btn.setOnClickListener { showDoubleButtonQuoteDialog() }
        activity_dialog.setOnClickListener { showActivityDialog() }
        progress_horizontal_dialog_btn.setOnClickListener { showHorizontalProgressDialog() }
        edit_dialog_btn.setOnClickListener { showInputDialog() }
        apply_permission_dialog_btn.setOnClickListener { showApplyPermissionDialog() }
        bottom_dialog_btn.setOnClickListener {
            showBottomDialog()

        }
    }

    override fun initData() {
    }

    /**
     * 单按钮弹窗
     */
    fun showOneButton() {
        DeiUiDialogFragmentHelper.createOneButtonDialog(context!!,
            "单行标题",
            "描述文字的字数最好控制在三行",
            "确定",
            true,
            View.OnClickListener { }
        ).show(fragmentManager, "单按钮")
    }

    /**
     * 双按钮弹窗
     */
    fun showDoubleButton() {
        val dialog = DeiUiDialogFragmentHelper.createDoubleButtonWithTitleDialog(
            context!!,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            "主操作",
            "辅助操作",
            true,
            object : DeiUiDialogFragmentHelper.OnDialogActionListener {
                override fun doCancelAction() {
                }

                override fun doOkAction() {
                }
            }
        )
        dialog.show(fragmentManager, "两个按钮")
    }

    /*
    *多操作按钮弹窗
    * withTitleMessage 是否带标题和描述信息
    * */
    fun showMultiBtnDialog(withTitleMessage: Boolean) {
        val dialog = DeiUiListItemDialogFragment(context!!)
            .apply {
                if (withTitleMessage) {
                    setTitle(this@FeedbackDialogFragment.getString(R.string.DialogTitle))
                    message = this@FeedbackDialogFragment.getString(R.string.DialogMessage)
                }
                addItem("操作1", object : DeiUiListItemDialogFragment.onSeparateItemClickListener {
                    override fun onClick() {
                    }
                })
                addItem("操作2", object : DeiUiListItemDialogFragment.onSeparateItemClickListener {
                    override fun onClick() {
                    }
                })
                addItem("操作3", object : DeiUiListItemDialogFragment.onSeparateItemClickListener {
                    override fun onClick() {

                    }
                })
            }
        dialog.show(fragmentManager, "列表")
    }

    /**
     * 带图弹窗
     */
    fun showDialogWithImg() {
        val dialog = DeiUiDialogFragmentHelper.createOneButtonWithImgDialog(
            context!!,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            getString(R.string.DialogPositiveText),
            true,
            View.OnClickListener { }
        )
        //设置图片资源
        dialog.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_dialog_big_temp))
        dialog.show(fragmentManager, "")
    }

    /**
     * 双操作按钮不带标题
     */
    fun showDoubleButtonWithoutTitle() {
        val dialog = DeiUiDialogFragmentHelper.createDoubleButtonWithoutTittleDialog(
            context!!,
            getString(R.string.DialogMessage),
            getString(R.string.DialogPositiveText),
            getString(R.string.DialogNegativeText),
            true,
            object : DeiUiDialogFragmentHelper.OnDialogActionListener {
                override fun doCancelAction() {

                }

                override fun doOkAction() {
                }
            }
        )
        dialog.show(fragmentManager, "双按钮不带标题")

    }

    /**
     * 双按钮带大图
     */
    fun showDoubleButtonWithBigImage() {
        val dialog = DeiUiDialogFragmentHelper.createDoubleButtonWithBigImgDialog(
            context!!,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            "主操作",
            "辅助操作",
            true,
            null
        )
        dialog.setDrawableRes(R.drawable.dialog_img)
        dialog.show(fragmentManager, "两个按钮")
    }

    /**
     * 双按钮引用弹窗
     */
    fun showDoubleButtonQuoteDialog() {
        val dialog = DeiUiDialogFragmentHelper.createDoubleButtonQuoteDialog(
            context!!,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            "主操作",
            "辅助操作",
            true,
            null
        )
        //设置引用弹窗图
        dialog.setDrawableRes(R.drawable.ic_dialog_medium_temp)
        dialog.show(fragmentManager, "引用弹窗")
    }

    /**
     * 申请授权弹窗
     */
    fun showApplyPermissionDialog() {
        val list = arrayListOf<String>()
        list.apply {
            for (i in 0 until 3)
                add("描述文字的字数最好控制在两行，右侧不能有标点符号")
        }
        val dialog =
            DeiUiUpgradeDialog(list, true, object : DeiUiUpgradeDialog.OnDialogActionListener {
                override fun onButtonClick() {

                }

                override fun onCancel() {
                }
            })
        dialog.setBtnStr(getString(R.string.DialogPositiveText))
        dialog.setTitle(getString(R.string.DialogTitle))
        dialog.setContentTitle("描述信息标题")
        dialog.setLogoDrawable(
            ContextCompat.getDrawable(
                context!!,
                R.drawable.ic_dialog_medium_temp
            )!!
        )
        dialog.show(fragmentManager, "申请授权弹窗")
    }

    /**
     * 活动运营弹窗
     */
    fun showActivityDialog() {
        val dialog =
            DeiUiDialogFragmentHelper.createActivityOperationDialog(
                context!!,
                "单行标题",
                "描述文字的字数最好控制在两行",
                "点击进入",
                true,
                View.OnClickListener { })
        //设置弹窗图
        dialog.setDrawableRes(R.drawable.dialog_img)
        dialog.show(fragmentManager, "活动运营弹窗")
    }

    /**
     * 水平进度条弹窗
     */
    fun showHorizontalProgressDialog() {
        val dialog = DeiUiDialogFragmentHelper.createHorizontalProgressDialog(
            context!!,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            "主操作",
            "辅助操作",
            50,
            null
        )
        dialog.show(fragmentManager, "两个按钮")
        dialog.setProgress(50)
    }

    /**
     * 输入弹窗
     */
    fun showInputDialog() {
        val dialog = DeiUiEditDialogHelper.createEditDialog(
            context!!,
            getString(R.string.DialogTitle),
            "请编辑信息",
            getString(R.string.confirm),
            getString(R.string.cancel),
            true,
            object : DeiUiEditDialogHelper.OnDialogActionListener {
                override fun doCancelAction() {

                }

                override fun doOkAction(msg: String?) {
                }
            })
        dialog.setEditTextMaxLength(15)
        dialog.setMessage("描述文字的字数最好控制在两行")
        dialog.show(fragmentManager, "输入弹窗")
    }

    /**
     * 底部弹窗
     */
    fun showBottomDialog() {
        DeiUiActionSheetDialog(context!!)
            .builder()
            .setCancelable(true)
            .setCancelColor(DeiUiActionSheetDialog.SheetItemColor.Blue)
            .setCanceledOnTouchOutside(true)
            .addSheetItem(
                "选项一", DeiUiActionSheetDialog.SheetItemColor.Blue
                , object : DeiUiActionSheetDialog.OnSheetItemClickListener {
                    override fun onClick(which: Int) {
                    }
                })
            .addSheetItem(
                "选项2",
                DeiUiActionSheetDialog.SheetItemColor.Blue,
                object : DeiUiActionSheetDialog.OnSheetItemClickListener {
                    override fun onClick(which: Int) {

                    }
                }).addSheetItem(
                "选项三",
                DeiUiActionSheetDialog.SheetItemColor.Red,
                object : DeiUiActionSheetDialog.OnSheetItemClickListener {
                    override fun onClick(which: Int) {

                    }
                }).addSheetItem(
                "选项四",
                DeiUiActionSheetDialog.SheetItemColor.Gray,
                object : DeiUiActionSheetDialog.OnSheetItemClickListener {
                    override fun onClick(which: Int) {

                    }
                })
            .show()
    }
}
